<?php

namespace App\Jobs;

use Exception;
use App\Models\User;
use App\Models\Notification;
use Illuminate\Bus\Queueable;
use App\Exceptions\FailedJobException;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Resources\NotificationResource;
use App\Events\Notifications\NotificationCreatedEvent;
use App\Events\Notifications\NotificationExceptionEvent;

class StoreNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, NotificationTypeValidation;

    private $user;

    private $game;

    private $notifiableUserId;

    private $notificationType;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param int $notifiableUserId
     * @param int $notificationType
     */
    public function __construct(User $user, int $notifiableUserId, int $notificationType)
    {
        $this->user = $user;
        $this->game = $user->acitveGame;
        $this->notifiableUserId = $notifiableUserId;
        $this->notificationType = $notificationType;

        $this->onQueue('notifications');
        $this->onConnection('database');
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws FailedJobException
     */
    public function handle()
    {
        if ($this->isNotificationTypeInvalid($this->notificationType)) {
            throw new FailedJobException('Notification type invalid!');
        }

        broadcast(new NotificationCreatedEvent(
            new NotificationResource(
                Notification::create([
                    'notification_type_id' => $this->notificationType,
                    'initiated_user_id' => $this->user->id,
                    'notifiable_user_id' => $this->notifiableUserId,
                ])
            )
        ));
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        broadcast(new NotificationExceptionEvent(
            $exception->getMessage(),
            $this->user->id
        ));
    }
}
