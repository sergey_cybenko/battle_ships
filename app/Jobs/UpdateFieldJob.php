<?php

namespace App\Jobs;

use App\Events\Games\FieldUpdatedEvent;
use Exception;
use App\Models\Cell;
use App\Models\Game;
use App\Models\Ship;
use App\Models\User;
use Illuminate\Bus\Queueable;
use App\Exceptions\FailedJobException;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Events\Notifications\NotificationExceptionEvent;
use Illuminate\Support\Collection;

class UpdateFieldJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;

    private $game;

    private $from;

    private $ship;

    private $oldCells;

    private $newCells;

    private $directionHorizontal;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param Game $game
     * @param int $shipId
     * @param bool|null $directionHorizontal
     * @param array|null $from
     */
    public function __construct(User $user, Game $game, int $shipId, ?bool $directionHorizontal, ?array $from)
    {
        $this->user = $user;
        $this->game = $game;
        $this->from = $from;
        $this->ship = Ship::find($shipId);
        $this->directionHorizontal = $directionHorizontal;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws FailedJobException
     */
    public function handle()
    {
        $this->isInvalid();

        $this->updateCells();

        broadcast(new FieldUpdatedEvent(
            $this->user->id,
            $this->oldCells,
            $this->newCells
        ));

    }

    /**
     * @return bool
     * @throws FailedJobException
     */
    private function isInvalid(): bool
    {
        if ($this->ship->game_id != $this->game->id) {
            return true;
        }

        if (!is_null($this->directionHorizontal) && !is_null($this->from)) {
            if ($this->directionHorizontal) {
                if ($this->isCoordinateInvalid('col', 'row')) {
                    throw new FailedJobException('Col out of range!');
                }
            } else {
                if ($this->isCoordinateInvalid('row', 'col')) {
                    throw new FailedJobException('Row out of range!');
                }
            }

            $cells = $this->game->cells()
                ->where('ship_id', '!=', $this->ship->id)
                ->where('user_id', $this->user->id)
                ->get(['col', 'row'])->toArray();

            if (
                in_array([
                    'col' => $this->from['col'] + 1,
                    'row' => $this->from['row'] + 1,
                ], $cells) ||
                in_array([
                    'col' => $this->from['col'],
                    'row' => $this->from['row'] + 1,
                ], $cells) ||
                in_array([
                    'col' => $this->from['col'] - 1,
                    'row' => $this->from['row'] + 1,
                ], $cells) ||
                in_array([
                    'col' => $this->from['col'] + 1,
                    'row' => $this->from['row'],
                ], $cells) ||
                in_array([
                    'col' => $this->from['col'],
                    'row' => $this->from['row'],
                ], $cells) ||
                in_array([
                    'col' => $this->from['col'] - 1,
                    'row' => $this->from['row'],
                ], $cells) ||
                in_array([
                    'col' => $this->from['col'] + 1,
                    'row' => $this->from['row'] - 1,
                ], $cells) ||
                in_array([
                    'col' => $this->from['col'],
                    'row' => $this->from['row'] - 1,
                ], $cells) ||
                in_array([
                    'col' => $this->from['col'] - 1,
                    'row' => $this->from['row'] - 1,
                ], $cells)
            ) {
                throw new FailedJobException('Ships should not cross!');
            };

            if ($this->directionHorizontal) {
                for ($i = 1; $i < $this->ship->shipTemplate->size; $i++) {
                    if (
                        in_array([
                            'col' => $this->from['col'] + $i + 1,
                            'row' => $this->from['row'] + 1,
                        ], $cells) ||
                        in_array([
                            'col' => $this->from['col'] + $i + 1,
                            'row' => $this->from['row'],
                        ], $cells) ||
                        in_array([
                            'col' => $this->from['col'] + $i + 1,
                            'row' => $this->from['row'] - 1,
                        ], $cells)
                    ) {
                        throw new FailedJobException('Ships should not cross!');
                    };
                }
            } else {
                for ($i = 1; $i < $this->ship->shipTemplate->size; $i++) {
                    if (
                        in_array([
                            'col' => $this->from['col'] + 1,
                            'row' => $this->from['row'] + $i + 1,
                        ], $cells) ||
                        in_array([
                            'col' => $this->from['col'],
                            'row' => $this->from['row'] + $i + 1,
                        ], $cells) ||
                        in_array([
                            'col' => $this->from['col'] - 1,
                            'row' => $this->from['row'] + $i + 1,
                        ], $cells)
                    ) {
                        throw new FailedJobException('Ships should not cross!');
                    };
                }
            }
        }

        return false;
    }

    /**
     * @param string $x
     * @param string $y
     * @return bool
     */
    private function isCoordinateInvalid(string $x = 'row', string $y = 'col'): bool
    {
        return !(
            0 < $this->from[$x] && $this->from[$x] <= $this->game["{$x}s"] &&
            0 < $this->from[$x] && ($this->from[$x] + $this->ship->shipTemplate->size - 1) <= $this->game["{$x}s"] &&
            0 < $this->from[$y] && $this->from[$y] <= $this->game["{$y}s"]
        );
    }

    private function updateCells(): void
    {
        $this->oldCells = $this->game->cells()->where([
            'user_id' => $this->user->id,
            'ship_id' => $this->ship->id,
        ])->get();

        if (is_null($this->from) || is_null($this->directionHorizontal)) {
            $this->deleteCells();
            return;
        }

        if ($this->directionHorizontal) {
            if ($this->oldCells->count()) {
                $this->updateNewCells('row', 'col');
            } else {
                $this->createNewCells('row', 'col');
            }
        } else {
            if ($this->oldCells->count()) {
                $this->updateNewCells('col', 'row');
            } else {
                $this->createNewCells('col', 'row');
            }
        }
    }

    /**
     * @param string $firstLine
     * @param string $secondLine
     */
    private function updateNewCells(string $firstLine = 'row', string $secondLine = 'col'): void
    {
        $this->newCells = $this->game->cells()->where([
            'user_id' => $this->user->id,
            'ship_id' => $this->ship->id,
        ])->get();

        foreach ($this->newCells as $index => $cell) {
            $cell->update([
                "$firstLine" => $this->from[$firstLine],
                "$secondLine" => $this->from[$secondLine] + $index,
            ]);
        }
    }

    /**
     * @param string $firstLine
     * @param string $secondLine
     */
    private function createNewCells(string $firstLine = 'row', string $secondLine = 'col'): void
    {
        $this->newCells = [];

        for ($i = 0; $i < $this->ship->shipTemplate->size; $i++) {
            $this->newCells[] = Cell::create([
                "$firstLine" => $this->from[$firstLine],
                "$secondLine" => $this->from[$secondLine] + $i,
                'user_id' => $this->user->id,
                'ship_id' => $this->ship->id,
                'game_id' => $this->game->id
            ]);
        }

        $this->newCells = new Collection($this->newCells);
    }

    private function deleteCells(): void
    {
        $this->game->cells()->where([
            'user_id' => $this->user->id,
            'ship_id' => $this->ship->id,
        ])->delete();

        $this->newCells = new Collection([]);
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        broadcast(new NotificationExceptionEvent(
            $exception->getMessage(),
            $this->user->id
        ));
    }
}
