<?php

namespace App\Jobs;

use Exception;
use App\Models\Game;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use App\Exceptions\FailedJobException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Events\Games\Creating\GameUpdatedEvent;
use App\Events\Notifications\NotificationExceptionEvent;

class UpdateActiveGameJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $rows;

    private $cols;

    private $game;

    private $shipList;

    /**
     * ActiveGameService constructor.
     * @param Game $game
     * @param FormRequest $request
     */
    public function __construct(Game $game, FormRequest $request)
    {
        $this->game = $game;
        $this->rows = $request->get('rows');
        $this->cols = $request->get('cols');
        $this->shipList = json_decode($this->game->shipList->data, true);

        $this->onQueue('notifications');
        $this->onConnection('database');
    }

    /**
     * @return int
     */
    private function getFieldArea(): int
    {
        return ($this->rows + 2) * ($this->cols + 2);
    }

    /**
     * Count a fields area
     *
     * @return int
     */
    private function getShipsArea(): int
    {
        $shipsArea = 0;

        foreach ($this->shipList as $item) {
            $shipsArea += ($item['size'] + 2) * 2 * $item['count'];
        }

        return $shipsArea;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws FailedJobException
     */
    public function handle()
    {
        if ($this->getFieldArea() < $this->getShipsArea()) {
            throw new FailedJobException('There is too much of ships');
        }

        $this->game->update([
            'rows' => $this->rows,
            'cols' => $this->cols,
        ]);

        broadcast(new GameUpdatedEvent($this->game));
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        broadcast(new NotificationExceptionEvent(
            $exception->getMessage(),
            $this->game->first_user_id
        ));
    }
}
