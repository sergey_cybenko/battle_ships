<?php

namespace App\Jobs;

use Exception;
use App\Models\Game;
use App\Models\User;
use App\Models\Message;
use Illuminate\Bus\Queueable;
use App\Events\Games\ShootEvent;
use App\Http\Resources\CellResource;
use Illuminate\Queue\SerializesModels;
use App\Exceptions\FailedJobException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Events\Games\Creating\GameUpdatedEvent;
use App\Events\Notifications\NotificationExceptionEvent;

class ShootJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $row;

    private $col;

    private $user;

    private $game;

    private $cell;

    private $message = '';

    private $gameEnded = false;

    private $shipCellsResource = null;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param Game $game
     * @param int $row
     * @param int $col
     */
    public function __construct(User $user, Game $game, int $row, int $col)
    {
        $this->user = $user;
        $this->game = $game;
        $this->row = $row;
        $this->col = $col;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws FailedJobException
     */
    public function handle()
    {
        switch (true) {
            case $this->userIsNotActive():
                throw new FailedJobException('There is not your turn now!');
            case $this->isShootInvalid():
                throw new FailedJobException('The cell was already fired!');
            case $this->gameEnded:
                $this->game->gameEnded();
                broadcast(new GameUpdatedEvent($this->game));
        }

        broadcast(new ShootEvent(
            $this->cell,
            $this->message,
            $this->shipCellsResource
        ));
    }

    private function userIsNotActive(): bool
    {
        if ($this->game->first_user_id === $this->user->id) {
            return !$this->game->first_user_active;
        }

        return $this->game->first_user_active;
    }

    private function isShootInvalid(): bool
    {
        $this->cell = $this->game->cells()->where([
            'row' => $this->row,
            'col' => $this->col,
            'user_id' => $this->game->first_user_id === $this->user->id ? $this->game->second_user_id : $this->game->first_user_id,
        ])->first();

        if (is_null($this->cell)) {
            $this->createCell();

            return false;
        } else if (is_null($this->cell->message_id)) {
            $this->updateCells();

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    private function getAllMessages(): array
    {
        return Message::get()->all();
    }

    /**
     * @param $shipCells
     * @return bool
     */
    private function isShipCellsDamaged($shipCells): bool
    {
        foreach ($shipCells as $shipCell) {
            if (is_null($shipCell->message_id)) {
                return false;
            }
        }

        return true;
    }

    private function createCell(): void
    {
        $messages = $this->getAllMessages();

        $this->cell = $this->game->cells()->create([
            'user_id' => $this->game->first_user_id === $this->user->id ? $this->game->second_user_id : $this->game->first_user_id,
            'row' => $this->row,
            'col' => $this->col,
            'message_id' => $messages[0]->id,
        ]);

        $this->message = $messages[$this->cell->message_id - 1]->content;
        $this->game->toggleActiveUser();
    }

    private function updateCells(): void
    {
        $messages = $this->getAllMessages();

        $shipCells = $this->game->cells()->where('ship_id', $this->cell->ship_id)
            ->where('id', '!=', $this->cell->id)->get();

        if ($this->isShipCellsDamaged($shipCells)) {
            $shipCells->push($this->cell);

            foreach ($shipCells as $shipCell) {
                $shipCell->update([
                    'message_id' => $messages[2]->id
                ]);
            }
            $this->shipCellsResource = CellResource::collection($shipCells);
        } else {
            $this->cell->update([
                'message_id' => $messages[1]->id
            ]);
        }

        $this->gameEnded = $this->game->cells()
                ->where('user_id', '!=', $this->user->id)
                ->where('message_id', '!=', null)
                ->where('ship_id', '!=', null)
                ->count() ==
            $this->game->cells()
                ->where('user_id', $this->user->id)
                ->where('ship_id', '!=', null)
                ->count();

        $this->message = $messages[$this->cell->message_id - 1]->content;
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        broadcast(new NotificationExceptionEvent(
            $exception->getMessage(),
            $this->user->id
        ));
    }
}
