<?php

namespace App\Jobs;

use Exception;
use App\Models\User;
use App\Models\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Resources\NotificationResource;
use App\Events\Notifications\NotificationExceptionEvent;
use App\Events\Notifications\NotificationBecomeIrrelevantEvent;

class DeleteNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;

    private $notification;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param Notification $notification
     */
    public function __construct(User $user, Notification $notification)
    {
        $this->user = $user;
        $this->notification = $notification;

        $this->onQueue('notifications');
        $this->onConnection('database');
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $notificationResource = new NotificationResource($this->notification);

        $this->notification->delete();

        broadcast(new NotificationBecomeIrrelevantEvent($notificationResource));
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        broadcast(new NotificationExceptionEvent(
            $exception->getMessage(),
            $this->user->id
        ));
    }
}
