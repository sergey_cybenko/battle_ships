<?php

namespace App\Jobs;

use Exception;
use App\Models\Game;
use App\Models\User;
use App\Models\ShipList;
use App\Models\Notification;
use Illuminate\Bus\Queueable;
use App\Exceptions\FailedJobException;
use App\Events\UserStatusChangedEvent;
use App\Events\Games\GameStartedEvent;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Resources\NotificationResource;
use App\Events\Games\Creating\GameUpdatedEvent;
use App\Events\Notifications\NotificationDeniedEvent;
use App\Events\Notifications\NotificationAcceptedEvent;
use App\Events\Notifications\NotificationExceptionEvent;

class UpdateNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, NotificationTypeValidation;

    private $user;

    private $game;

    private $accept;

    private $notification;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param Notification $notification
     * @param bool $accept
     */
    public function __construct(User $user, Notification $notification, bool $accept)
    {
        $this->user = $user;
        $this->game = $user->acitveGame;
        $this->accept = $accept;
        $this->notification = $notification;

        $this->onQueue('notifications');
        $this->onConnection('database');
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     * @throws FailedJobException
     */
    public function handle()
    {
        if ($this->isNotificationTypeInvalid($this->notification->notification_type_id)) {
            throw new FailedJobException('Notification type invalid!');
        } else if (isset($this->notification->accept)) {
            throw new FailedJobException('Notification invalid!');
        }

        $notificationResource = new NotificationResource($this->notification);
        $this->notification->update(['accept' => $this->accept]);

        if ($this->accept) {
            $this->handleAcceptedNotification();

            broadcast(new NotificationAcceptedEvent($notificationResource));
            broadcast(new UserStatusChangedEvent($this->game->firstUser));
            broadcast(new UserStatusChangedEvent($this->game->secondUser));
        } else {
            broadcast(new NotificationDeniedEvent($notificationResource));
        }
    }

    private function handleAcceptedNotification()
    {
        $handler = [
            1 => 'gameStarted',
            2 => 'gameEndedUserSurrender',
            3 => 'gameEndedDraw'
        ][$this->notification->notification_type_id];

        $this->$handler();
    }

    private function gameStarted()
    {
        $this->game = Game::create([
            'first_user_id' => $this->notification->initiated_user_id,
            'second_user_id' => $this->notification->notifiable_user_id,
            'ship_list_id' => (factory(ShipList::class)->create())->id
        ])->fresh()->setAppends(['firstUser', 'secondUser']);
        $this->game->gameStarted();

        broadcast(new GameStartedEvent($this->game));
    }

    private function gameEndedDraw()
    {
        $this->game->update([
            'first_user_active' => null
        ]);

        $this->game->gameEnded();
        broadcast(new GameUpdatedEvent($this->game));
    }

    private function gameEndedUserSurrender()
    {
        $this->game->update([
            'first_user_active' => $this->game->first_user_id === $this->user->id
        ]);

        $this->game->gameEnded();
        broadcast(new GameUpdatedEvent($this->game));
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        broadcast(new NotificationExceptionEvent(
            $exception->getMessage(),
            $this->user->id
        ));
    }
}
