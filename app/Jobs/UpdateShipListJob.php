<?php

namespace App\Jobs;

use Exception;
use App\Models\Game;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use App\Exceptions\FailedJobException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Events\Games\Creating\GameUpdatedEvent;
use App\Events\Notifications\NotificationExceptionEvent;

class UpdateShipListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $game;

    private $count;

    private $shipList;

    private $ship_template_id;

    public function __construct(Game $game, FormRequest $request)
    {
        $this->game = $game;
        $this->count = $request->get('count');
        $this->ship_template_id = $request->get('ship_template_id');

        $this->setShipList();

        $this->onQueue('notifications');
        $this->onConnection('database');
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws FailedJobException
     */
    public function handle()
    {
        if ($this->getFieldArea() < $this->getShipsArea()) {
            throw new FailedJobException('There is too much of ships');
        }

        $this->game->shipList->update(['data' => $this->getEncodedShipList()]);

        broadcast(new GameUpdatedEvent($this->game));
    }

    private function getFieldArea(): int
    {
        return ($this->game->cols + 2) * ($this->game->rows + 2);
    }

    /**
     * Set the ship list data
     */
    private function setShipList(): void
    {
        $shipList = json_decode($this->game->shipList->data, true);

        $shipTemplateId = $this->ship_template_id;
        $count = $this->count;

        foreach ($shipList as $index => $item) {
            if ($item['id'] === $shipTemplateId) {
                $shipList[$index]['count'] = $count;

                break;
            }
        }

        $this->shipList = $shipList;
    }

    /**
     * @return string
     */
    private function getEncodedShipList(): string
    {
        return json_encode($this->shipList);
    }

    /**
     * Count a fields area
     *
     * @return int
     */
    private function getShipsArea(): int
    {
        $shipsArea = 0;

        foreach ($this->shipList as $item) {
            $shipsArea += ($item['size'] + 2) * 2 * $item['count'];
        }

        return $shipsArea;
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        broadcast(new NotificationExceptionEvent(
            $exception->getMessage(),
            $this->game->first_user_id
        ));
    }
}
