<?php

namespace App\Jobs;


trait NotificationTypeValidation
{
    /**
     * @param int $notificationType
     * @return bool
     */
    protected function isNotificationTypeInvalid(int $notificationType): bool
    {
        $validate = [
            1 => 'isUserNotInGame',
            2 => 'isUserInGame',
            3 => 'isUserInGame'
        ][$notificationType];

        return $this->$validate();
    }

    /**
     * @return bool
     */
    protected function isUserInGame(): bool
    {
        return is_null($this->game) && $this->user->isNotInGame();
    }

    /**
     * @return bool
     */
    protected function isUserNotInGame(): bool
    {
        return isset($this->game) && $this->user->isInGame();
    }
}