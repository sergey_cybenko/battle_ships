<?php

namespace App\Jobs;

use Exception;
use App\Models\Ship;
use App\Models\Game;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Redis;
use App\Exceptions\FailedJobException;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Events\Games\Creating\GameUpdatedEvent;
use App\Events\Games\Creating\UserReadyChangedEvent;
use App\Events\Notifications\NotificationExceptionEvent;

class ToggleUserReadyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;

    private $game;

    private $channel;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param Game $game
     */
    public function __construct(User $user, Game $game)
    {
        $this->user = $user;
        $this->game = $game;

        $this->channel = str_replace('{game}', $this->game->id, [
            'presence-game.configure.{game}:members',
            'presence-field.configure.{game}:members',
        ][$this->game->status]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->isGameInvalid();

        $this->userToggleReady();

        if ($this->isUsersReady()) {
            $this->prepareToNextStatus();
            broadcast(new GameUpdatedEvent($this->game));
        } else {
            broadcast(new UserReadyChangedEvent($this->user, $this->game));
        }

    }

    /**
     * Validate selected logic of app
     *
     * @return bool
     */
    private function isGameInvalid(): bool
    {
        if ($this->user->ready) {
            return false;
        }

        $validate = [
            'isShipsCountMoreThenZero',
            'isAllShipsGotCoords',
        ][$this->game->status];

        return !$this->$validate();
    }

    /**
     * @return bool
     * @throws FailedJobException
     */
    private function isShipsCountMoreThenZero(): bool
    {
        $shipsCount = 0;

        foreach (json_decode($this->game->shipList->data) as $shipTemplate) {
            $shipsCount += $shipTemplate->count;
        }

        if ($shipsCount == 0) {
            throw new FailedJobException('At least one ship is required!');
        }

        return true;
    }

    /**
     * @return bool
     * @throws FailedJobException
     */
    private function isAllShipsGotCoords(): bool
    {
        foreach ($this->game->getShipsRecords($this->user->id) as $ship) {
            if ($ship->count == 0) {
                throw new FailedJobException('All ships should have coordinates!');
            }
        }
        return true;
    }

    /**
     * Toggle users ready field
     */
    private function userToggleReady()
    {
        $this->user->toggleReady();

        $this->updateRedis();
    }

    /**
     * Update user.ready filed in Redis presence channel
     */
    private function updateRedis()
    {
        $redis = json_decode(Redis::get($this->channel));

        if ($redis[0]->user_info->id == $this->user->id) {
            $redis[0]->user_info->ready = $this->user->ready;
        } else {
            $redis[1]->user_info->ready = $this->user->ready;
        }
        Redis::set($this->channel, json_encode($redis));
    }

    /**
     * @return bool
     */
    private function isUsersReady(): bool
    {
        return $this->game->isUsersReady();
    }

    private function prepareToNextStatus()
    {
        $method = [
            'generateShipsFromShipList',
            'setFirstUserActiveField'
        ][$this->game->status - 1];

        $this->$method();
    }

    /**
     * Store ships to DB from ship list
     */
    private function generateShipsFromShipList()
    {
        echo "Generating Ships\n";
        foreach (json_decode($this->game->shipList->data) as $shipTemplate) {
            for ($i = 0; $i < $shipTemplate->count; $i++) {
                Ship::insert([
                    [
                        'user_id' => $this->game->first_user_id,
                        'game_id' => $this->game->id,
                        'ship_template_id' => $shipTemplate->id,
                    ],
                    [
                        'user_id' => $this->game->second_user_id,
                        'game_id' => $this->game->id,
                        'ship_template_id' => $shipTemplate->id,
                    ]
                ]);
            }
        }
    }

    private function setFirstUserActiveField()
    {
        $this->game->update([
            'first_user_active' => true
        ]);
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        broadcast(new NotificationExceptionEvent(
            $exception->getMessage(),
            $this->user->id
        ));
    }
}
