<?php

namespace App\Policies;

use App\Models\Game;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GamePolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Game $game
     * @return bool
     */
    public function configureGame(User $user, Game $game): bool
    {
        if ($game->status == 0 && $user->status == 1) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Game $game
     * @return bool
     */
    public function configureField(User $user, Game $game): bool
    {
        if ($game->status == 1 && $user->status == 1) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Game $game
     * @return bool
     */
    public function configure(User $user, Game $game): bool
    {
        if (($game->status == 0 || $game->status == 1) && $user->status == 1) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Game $game
     * @return bool
     */
    public function isInitiator(User $user, Game $game): bool
    {
        if ($game->first_user_id == $user->id) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Game $game
     * @return bool
     */
    public function shoot(User $user, Game $game): bool
    {
        if ($game->status === 2) {
            return true;
        }

        if ($user->id == $game->first_user_id) {
            return $game->first_user_active === true;
        } else if ($user->id == $game->second_user_id) {
            return $game->first_user_active === false;
        }

        return false;
    }

    public function before(User $user, string $policy, Game $game)
    {
        return !is_null($game);
    }
}
