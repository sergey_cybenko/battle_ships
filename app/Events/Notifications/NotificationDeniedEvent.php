<?php

namespace App\Events\Notifications;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NotificationDeniedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels, NotificationEvent;
}
