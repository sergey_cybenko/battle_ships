<?php

namespace App\Events\Notifications;

use App\Events\BroadcastAsClassName;
use App\Http\Resources\NotificationResource;
use Illuminate\Broadcasting\PresenceChannel;

trait NotificationEvent
{
    use BroadcastAsClassName;

    public $notification;

    /**
     * Create a new event instance.
     *
     * @param $notification
     */
    public function __construct(NotificationResource $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [
            new PresenceChannel('notifications.user.' . $this->notification->notifiable_user_id),
            new PresenceChannel('notifications.user.' . $this->notification->initiated_user_id),
        ];
    }
}
