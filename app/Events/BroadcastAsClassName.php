<?php

namespace App\Events;


trait BroadcastAsClassName
{
    /**
     * @return string
     * @throws \ReflectionException
     */
    public function broadcastAs(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}
