<?php

namespace App\Events\Games;

use App\Models\Cell;
use App\Events\BroadcastAsClassName;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ShootEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels, BroadcastAsClassName;

    public $cells;

    public $message;

    public $cell;

    private $channel;

    /**
     * Create a new event instance.
     *
     * @param Cell $cell
     * @param string $message
     * @param AnonymousResourceCollection|null $cells
     */
    public function __construct(Cell $cell, string $message, ?AnonymousResourceCollection $cells)
    {
        $this->cells = $cells;

        $this->message = $message;

        $this->cell = $cell;

        $this->channel = 'active.game.' . $cell->game_id;

        $this->dontBroadcastToCurrentUser();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel($this->channel);
    }
}
