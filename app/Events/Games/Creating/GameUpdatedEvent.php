<?php

namespace App\Events\Games\Creating;

use App\Events\Games\GameEvent;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;


class GameUpdatedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels, GameEvent;

    private $channels = [
        'game.configure.{game}',
        'game.configure.{game}',
        'field.configure.{game}',
        'active.game.{game}',
    ];

    public function broadcastOn()
    {
        return [
            new PresenceChannel(str_replace(
                '{game}',
                $this->game['id'],
                $this->channels[$this->game['status']]
            )),
            new PresenceChannel('dashboard'),
        ];
    }
}
