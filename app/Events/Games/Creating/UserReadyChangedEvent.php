<?php

namespace App\Events\Games\Creating;

use App\Events\BroadcastAsClassName;
use App\Models\Game;
use App\Models\User;
use App\Http\Resources\UserResource;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;


class UserReadyChangedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels, BroadcastAsClassName;

    public $user;

    protected $channel;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Game $game
     */
    public function __construct(User $user, Game $game)
    {
        $this->channel = $game->status == 0 ? 'game.configure.{game}' : 'field.configure.{game}';
        $this->channel = str_replace('{game}', $game->id, $this->channel);
        $this->user = new UserResource($user);
        $this->dontBroadcastToCurrentUser();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|\Illuminate\Broadcasting\Channel[]|PresenceChannel
     */
    public function broadcastOn()
    {
        return new PresenceChannel($this->channel);
    }
}
