<?php

namespace App\Events\Games;

use Illuminate\Support\Collection;
use App\Http\Resources\CellResource;
use App\Events\BroadcastAsClassName;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FieldUpdatedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels, BroadcastAsClassName;

    public $cells;

    private $channel;

    public $deleted_cells;

    /**
     * Create a new event instance.
     *
     * @param int $userId
     * @param Collection $deletedCells
     * @param Collection $cells
     */
    public function __construct(int $userId, Collection $deletedCells, Collection $cells)
    {
        $this->deleted_cells = CellResource::collection($deletedCells);
        $this->channel = 'notifications.user.' . $userId;
        $this->cells = CellResource::collection($cells);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel($this->channel);
    }
}
