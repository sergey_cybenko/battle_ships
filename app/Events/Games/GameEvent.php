<?php

namespace App\Events\Games;

use App\Events\BroadcastAsClassName;
use App\Models\Game;
use App\Http\Resources\GameResource;
use Illuminate\Broadcasting\PresenceChannel;

trait GameEvent
{
    use BroadcastAsClassName;

    public $game;

    /**
     * Create a new event instance.
     *
     * @param Game $game
     */
    public function __construct(Game $game)
    {
        $this->game = new GameResource($game);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('dashboard');
    }
}
