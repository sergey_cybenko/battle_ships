<?php

namespace App\Services;

use App\Http\Resources\CellResource;
use App\Models\Cell;

trait BuildField
{
    protected $game;

    protected $user;

    protected $cells = [];

    /**
     * Build game field from rows and cols
     */
    protected function buildField(): void
    {
        for ($row = 1; $row <= $this->game->rows; $row++) {
            if (isset($this->cells[$row])) {
                $this->cells[$row] = [];
            }
            for ($col = 1; $col <= $this->game->cols; $col++) {
                $this->cells[$row][$col]['type'] = 0;
                $this->cells[$row][$col]['ships'] = [];
            }
        }
    }

    /**
     * @param iterable $cells
     */
    protected function addCellsToField(iterable $cells): void
    {
        foreach ($cells as $cell) {
            $this->cells[$cell->row][$cell->col] = new CellResource($cell);

            $row = $cell->row - 1;
            $this->updateRowNearShip($row, $cell);

            $row = $cell->row + 1;
            $this->updateRowNearShip($row, $cell);

            $this->updateColsNearShip($cell->ship_id, $this->game->cols, $cell->row, $cell->col);
        }
    }

    /**
     * @param int $row
     * @param Cell $cell
     */
    protected function updateRowNearShip(int $row, Cell $cell): void
    {
        if (0 < ($row) && ($row) <= $this->game->rows) {
            $this->updateColsNearShip($cell->ship_id, $this->game->cols, $row, $cell->col);
            if (is_array($this->cells[$row][$cell->col]) && isset($cell->ship_id)) {
                $this->cells[$row][$cell->col]['type'] = 1;
                if (!in_array($cell->ship_id, $this->cells[$row][$cell->col]['ships'])) {
                    $this->cells[$row][$cell->col]['ships'][] = $cell->ship_id;
                }
            }
        }
    }

    /**
     * @param int $ship_id
     * @param int $cols
     * @param int $row
     * @param int $col
     */
    protected function updateColsNearShip(?int $ship_id, int $cols, int $row, int $col): void
    {
        $colDecr = $col - 1;
        $this->updateColNearShip($ship_id, $cols, $row, $colDecr);

        $colIncr = $col + 1;
        $this->updateColNearShip($ship_id, $cols, $row, $colIncr);
    }

    /**
     * @param int $ship_id
     * @param int $cols
     * @param int $row
     * @param int $col
     */
    protected function updateColNearShip(?int $ship_id, int $cols, int $row, int $col): void
    {
        if (0 < ($col) && ($col) <= $cols) {
            if (is_array($this->cells[$row][$col]) && isset($ship_id)) {
                $this->cells[$row][$col]['type'] = 1;
                if (!in_array($ship_id, $this->cells[$row][$col]['ships'])) {
                    $this->cells[$row][$col]['ships'][] = $ship_id;
                }
            }
        }
    }
}
