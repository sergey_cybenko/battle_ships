<?php

namespace App\Services;

use App\Models\Game;
use App\Models\User;
use Illuminate\Support\Collection;
use \App\Services\Interfaces\FieldInterface;

class FieldService implements FieldInterface
{
    use BuildField;

    public function __construct(Game $game, User $user)
    {
        $this->game = $game;
        $this->user = $user;
    }

    public function getCells(): array
    {
        $this->buildField();
        $this->addCellsToField($this->game->cells()->where('user_id', $this->user->id)->get());

        return $this->cells;
    }

    public function getGame(): Game
    {
        return $this->game;
    }

    public function getGameShips(): Collection
    {
        return $this->game->getShipsRecords($this->user->id);
    }
}
