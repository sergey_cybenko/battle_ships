<?php

namespace App\Services\Interfaces;

use App\Models\Game;
use App\Models\User;
use Illuminate\Support\Collection;

interface FieldInterface
{

    /**
     * FieldService constructor.
     * @param Game $game
     * @param User $user
     */
    public function __construct(Game $game, User $user);

    /**
     * @return Game
     */
    public function getGame(): Game;

    /**
     * @return array
     */
    public function getCells(): array;

    /**
     * @return Collection
     */
    public function getGameShips(): Collection;
}
