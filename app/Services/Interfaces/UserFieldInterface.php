<?php

namespace App\Services\Interfaces;

use App\Models\Game;
use App\Models\User;

interface UserFieldInterface
{
    /**
     * UserActiveGameInterface constructor.
     * @param User $user
     * @param Game $game
     */
    public function __construct(User $user, Game $game);

    /**
     * @return array
     */
    public function getFirstUserCells(): array;

    /**
     * @return array
     */
    public function getSecondUserCells(): array;

    /**
     * @return array
     */
    public function getScore(): array;

    /**
     * @return bool
     */
    public function isOwnField(): bool;
}