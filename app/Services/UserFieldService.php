<?php

namespace App\Services;

use App\Models\Game;
use App\Models\User;
use Illuminate\Support\Collection;
use App\Services\Interfaces\UserFieldInterface;

class UserFieldService implements UserFieldInterface
{
    use BuildField;

    private $ownField = false;

    public function __construct(User $user, Game $game)
    {
        $this->user = $user;
        $this->game = $game;
    }

    public function getFirstUserCells(): array
    {
        return $this->getCellsFrom('first_user_id');
    }

    public function getSecondUserCells(): array
    {
        return $this->getCellsFrom('second_user_id');
    }

    /**
     * @param string $from
     * @return Collection
     */
    private function getUserCells(string $from): Collection
    {
        return $this->game->cells()
            ->where('user_id', $this->game->$from)
            ->where('message_id', '!=', null)->with('message')->get();
    }

    /**
     * @param string $from
     * @return Collection
     */
    private function getOwnCells(string $from): Collection
    {
        return $this->game->cells()
            ->where('user_id', $this->game->$from)->with('message')->get();
    }

    public function isOwnField(): bool
    {
        return $this->ownField;
    }

    /**
     * @param string $from
     * @return array
     */
    private function getCellsFrom(string $from): array
    {
        $this->buildField();

        if ($this->user->id === $this->game->$from) {
            $this->ownField = true;
            $this->addCellsToField($this->getOwnCells($from));
        } else {
            $this->ownField = false;
            $this->addCellsToField($this->getUserCells($from));
        }
        return $this->cells;
    }

    public function getScore(): array
    {
        return [
            'first_user_score' => $this->game->cells()->where([
                'user_id' => $this->game->second_user_id,
            ])->whereNotNull('ship_id')->whereNotNull('message_id')->count(),
            'score' => $this->game->cells()->where([
                'user_id' => $this->game->first_user_id,
            ])->whereNotNull('ship_id')->count(),
            'second_user_score' => $this->game->cells()->where([
                'user_id' => $this->game->first_user_id,
            ])->whereNotNull('ship_id')->whereNotNull('message_id')->count(),
        ];
    }
}