<?php

namespace App\Http\Controllers\Api;

use App\Jobs\ShootJob;
use App\Http\Controllers\Controller;
use App\Http\Requests\Games\ShootRequest;

class ShootController extends Controller
{
    /**
     * @param ShootRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ShootRequest $request)
    {
        dispatch(new ShootJob(
            $request->getUser(),
            $request->getGame(),
            $request->get('row'),
            $request->get('col')
        ));

        return response()->json();
    }
}
