<?php

namespace App\Http\Controllers\Api;

use App\Models\Notification;
use Illuminate\Http\Request;
use App\Jobs\StoreNotificationJob;
use App\Jobs\UpdateNotificationJob;
use App\Jobs\DeleteNotificationJob;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\Notifications\StoreNotificationRequest;
use App\Http\Requests\Notifications\UpdateNotificationRequest;

class NotificationController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreNotificationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNotificationRequest $request)
    {
        dispatch(new StoreNotificationJob(
            $request->user(),
            $request->get('notifiable_user_id'),
            $request->get('notification_type', 1)
        ));

        return response()->json();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateNotificationRequest $request
     * @param Notification $notification
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNotificationRequest $request, Notification $notification)
    {
        if ($request->user()->id !== $notification->notifiable_user_id) {
            throw (new ModelNotFoundException)->setModel(
                get_class($notification),
                [$notification->notifiable_user_id]
            );
        }

        dispatch(new UpdateNotificationJob(
            $request->user(),
            $notification,
            $request->get('accept')
        ));

        return response()->json();
    }

    /**
     * @param Request $request
     * @param Notification $notification
     * @return void
     */
    public function destroy(Request $request, Notification $notification)
    {
        if ($request->user()->id !== $notification->initiated_user_id) {
            throw (new ModelNotFoundException)->setModel(
                get_class($notification),
                [$notification->initiated_user_id]
            );
        }

        dispatch(new DeleteNotificationJob(
            $request->user(),
            $notification
        ));
    }
}
