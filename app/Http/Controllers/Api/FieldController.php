<?php

namespace App\Http\Controllers\Api;

use App\Models\Game;
use App\Jobs\UpdateFieldJob;
use Illuminate\Http\Request;
use App\Services\FieldService;
use App\Http\Requests\FieldRequest;
use App\Http\Resources\ShipResource;
use App\Http\Resources\GameResource;
use App\Http\Controllers\Controller;

class FieldController extends Controller
{
    /**
     * @param Request $request
     * @param Game $game
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Game $game)
    {
        $service = new FieldService($game, $request->user());

        return response()->json([
            'game' => new GameResource($service->getGame()),
            'ships' => ShipResource::collection($service->getGameShips()),
            'cells' => $service->getCells()
        ]);
    }

    /**
     * @param FieldRequest $request
     * @param Game $game
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(FieldRequest $request, Game $game)
    {
        dispatch(new UpdateFieldJob(
            $request->user(),
            $game,
            $request->get('ship_id'),
            $request->get('direction_horizontal'),
            $request->get('from')
        ));

        return response()->json();
    }
}
