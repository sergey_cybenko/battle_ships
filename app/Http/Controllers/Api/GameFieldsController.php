<?php

namespace App\Http\Controllers\Api;

use App\Models\Game;
use Illuminate\Http\Request;
use App\Services\UserFieldService;
use App\Http\Resources\GameResource;
use App\Http\Controllers\Controller;

class GameFieldsController extends Controller
{
    /**
     * @param Request $request
     * @param Game $game
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Game $game)
    {
        $service = new UserFieldService($request->user(), $game);

        return response()->json([
            'game' => new GameResource($game),
            'first_user_cells' => $service->getFirstUserCells(),
            'is_first_user_owner' => $service->isOwnField(),
            'second_user_cells' => $service->getSecondUserCells(),
            'is_second_user_owner' => $service->isOwnField(),
            'score' => $service->getScore(),
        ]);
    }
}
