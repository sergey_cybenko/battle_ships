<?php

namespace App\Http\Controllers\Api;

use App\Models\Game;
use Illuminate\Http\Request;
use App\Jobs\UpdateActiveGameJob;
use App\Http\Resources\GameResource;
use App\Http\Controllers\Controller;
use App\Http\Requests\Games\UpdateGameRequest;

class ActiveGameController extends Controller
{
    /**
     * @param Request $request
     * @param Game $game
     * @return GameResource
     */
    public function show(Request $request, Game $game)
    {
        return new GameResource($game);
    }

    /**
     * @param UpdateGameRequest $request
     * @param Game $game
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateGameRequest $request, Game $game)
    {
        dispatch(new UpdateActiveGameJob($game, $request));

        return response()->json();
    }
}
