<?php

namespace App\Http\Controllers\Api;

use App\Jobs\ToggleUserReadyJob;
use App\Http\Controllers\Controller;
use App\Http\Requests\ToggleUserReadyRequest;

class ToggleUserReadyController extends Controller
{
    /**
     * @param ToggleUserReadyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ToggleUserReadyRequest $request)
    {
        dispatch(new ToggleUserReadyJob(
            $request->getUser(),
            $request->getGame()
        ));

        return response()->json();
    }
}
