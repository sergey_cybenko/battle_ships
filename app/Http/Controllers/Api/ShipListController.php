<?php

namespace App\Http\Controllers\Api;

use App\Models\Game;
use App\Jobs\UpdateShipListJob;
use App\Http\Controllers\Controller;
use App\Http\Requests\Games\UpdateShipListRequest;

class ShipListController extends Controller
{
    /**
     * @param UpdateShipListRequest $request
     * @param Game $game
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateShipListRequest $request, Game $game)
    {
        dispatch(new UpdateShipListJob($game, $request));

        return response()->json();
    }
}
