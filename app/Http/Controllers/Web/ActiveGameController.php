<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActiveGameController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $activeGame = $user->activeGame;

        switch (true) {
            case is_null($activeGame):
                return redirect()->route('dashboard.index');
            case $activeGame->status === 0:
                return view('game.configure')->with(['user' => $user]);
            case $activeGame->status === 1:
                return view('field.configure')->with(['user' => $user]);
            default:
                return view('active-game.index')->with(['user' => $user]);
        }
    }
}
