<?php

namespace App\Http\Requests\Games;

use Illuminate\Foundation\Http\FormRequest;

class UpdateGameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();
        $game = $user->activeGame;

        return $user->can('isInitiator', $game) || $user->can('configureGame', $game);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rows' => 'required|numeric|min:5|max:30',
            'cols' => 'required|numeric|min:5|max:30',
        ];
    }
}
