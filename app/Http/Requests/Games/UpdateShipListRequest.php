<?php

namespace App\Http\Requests\Games;

use Illuminate\Foundation\Http\FormRequest;

class UpdateShipListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();
        $game = $user->activeGame;

        return $user->can('isInitiator', $game) || $user->can('configureGame', $game);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ship_template_id' => 'required|numeric|exists:ship_templates,id',
            'count' => 'required|numeric|min:0'
        ];
    }
}
