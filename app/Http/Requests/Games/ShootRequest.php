<?php

namespace App\Http\Requests\Games;

use App\Models\Game;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class ShootRequest extends FormRequest
{
    private $game;

    private $user;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->user = $this->user();
        $this->game = $this->user->activeGame;

        return $this->user->can('shoot', $this->game);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'col' => [
                'required',
                'numeric',
                'min:0',
                'max:' . $this->game->cols
            ],
            'row' => [
                'required',
                'numeric',
                'min:0',
                'max:' . $this->game->rows
            ],
        ];
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Game
     */
    public function getGame(): Game
    {
        return $this->game;
    }
}
