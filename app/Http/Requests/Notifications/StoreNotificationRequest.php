<?php

namespace App\Http\Requests\Notifications;

use App\Rules\UserIsOnline;
use App\Rules\ExceptAuthUser;
use App\Rules\NotificationNotSent;
use Illuminate\Foundation\Http\FormRequest;

class StoreNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'notifiable_user_id' => [
                'required',
                'numeric',
                'exists:users,id',
                new UserIsOnline,
                new ExceptAuthUser,
                new NotificationNotSent,
            ],
            'type' => [
                'numeric',
                'exists:notification_types,id',
            ]
        ];
    }
}
