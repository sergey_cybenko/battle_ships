<?php

namespace App\Http\Requests;

use App\Models\Game;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class ToggleUserReadyRequest extends FormRequest
{
    private $user;

    private $game;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->user = $this->user();
        $this->game = $this->user->activeGame;

        return $this->user->can('configure', $this->game);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Game
     */
    public function getGame(): Game
    {
        return $this->game;
    }
}
