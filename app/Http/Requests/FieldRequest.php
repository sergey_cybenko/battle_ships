<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FieldRequest extends FormRequest
{
    protected $game;

    protected $user;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->user = $this->user();
        $this->game = $this->user->activeGame;

        return $this->user->can('configureField', $this->game);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rows = $this->game->rows;
        $cols = $this->game->cols;
        return [
            'ship_id' => 'required|exists:ships,id',
            'direction_horizontal' => 'boolean',
            'from.row' => "numeric|min:1|max:$rows",
            'from.col' => "numeric|min:1|max:$cols",
        ];
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getGame()
    {
        return $this->game;
    }
}
