<?php

namespace App\Http\Resources;

use App\Models\Ship;
use App\Models\ShipTemplate;
use Illuminate\Http\Resources\Json\JsonResource;

class ShipResource extends JsonResource
{
    protected $ship;

    /**
     * ShipResource constructor.
     * @param \stdClass $ship
     */
    public function __construct(\stdClass $ship)
    {
        $this->ship = $ship;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->ship->id,
            'ship_template_id' => $this->ship->ship_template_id,
            'size' => $this->ship->size,
            'name' => $this->ship->name,
            'free' => $this->ship->count == 0,
        ];
    }
}
