<?php

namespace App\Http\Resources;

use App\Models\Notification;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    protected $notification;

    public function __construct(Notification $notification)
    {
        $this->notification = $notification;

        parent::__construct($notification);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->notification->id,
            'notification_type_id' => $this->notification->notification_type_id,
            'initiated_user_id' => $this->notification->initiated_user_id,
            'notifiable_user_id' => $this->notification->notifiable_user_id,
        ];
    }
}
