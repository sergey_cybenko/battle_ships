<?php

namespace App\Http\Resources;

use App\Models\Cell;
use Illuminate\Http\Resources\Json\JsonResource;

class CellResource extends JsonResource
{
    protected $cell;

    public function __construct(Cell $cell)
    {
        $this->cell = $cell;
        parent::__construct($cell);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->cell->id,
            'row' => $this->cell->row,
            'col' => $this->cell->col,
            'user_id' => $this->cell->user_id,
            'ship_id' => $this->cell->ship_id,
            'ships' => isset($this->cell->ship_id) ? [$this->cell->ship_id] : [],
            'message' => is_null($this->cell->message) ? '' : $this->cell->message->content,
            'type' => is_null($this->cell->message_id) ? 2 : 3,
        ];
    }
}
