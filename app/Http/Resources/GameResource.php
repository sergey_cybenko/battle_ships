<?php

namespace App\Http\Resources;

use App\Models\Game;
use Illuminate\Http\Resources\Json\JsonResource;

class GameResource extends JsonResource
{
    protected $game;

    public function __construct(Game $game)
    {
        $this->game = $game;
        parent::__construct($game);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->game->id,
            'first_user' => new UserResource($this->game->firstUser),
            'second_user' => new UserResource($this->game->secondUser),
            'first_user_active' => (bool) $this->game->first_user_active,
            'status' => $this->game->status,
            'ship_list' => is_null($this->game->ship_list_id) ? null : json_decode($this->game->shipList->data),
            'rows' => $this->game->rows,
            'cols' => $this->game->cols,
        ];
    }
}
