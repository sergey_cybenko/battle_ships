<?php

namespace App\Rules;

use App\Models\Notification;
use Illuminate\Contracts\Validation\Rule;

class NotificationNotSent implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return is_null(Notification::where('accept', null)
            ->where('notifiable_user_id', $value)->where('initiated_user_id', auth()->user()->id)->first());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Notification has already sent.';
    }
}
