<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipTemplate extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ship_templates';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'size'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the ship for this model.
     *
     * @return App\Models\Ship
     */
    public function ship()
    {
        return $this->hasOne('App\Models\Ship', 'ship_template_id', 'id');
    }
}
