<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipList extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ship_lists';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'data',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the Game for this model.
     *
     * @return App\Models\Game
     */
    public function Game()
    {
        return $this->belongsTo('App\Models\Game', 'game_id', 'id');
    }

    /**
     * Get the User for this model.
     *
     * @return App\Models\User
     */
    public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
