<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'ready', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token', 'api_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'ready' => 'boolean'
    ];

    protected $appends = [
        'active_game'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function createdGames()
    {
        return $this->hasMany(Game::class, 'first_user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invitedGames()
    {
        return $this->hasMany(Game::class, 'second_user_id', 'id');
    }

    /**
     * @return mixed
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class, 'notifiable_user_id')->orderBy('created_at', 'desc');
    }

    /**
     * @return mixed
     */
    public function initiatedNotifications()
    {
        return $this->hasMany(Notification::class, 'initiated_user_id')->orderBy('created_at', 'desc');
    }

    /**
     * @return mixed
     */
    public function getActiveGameAttribute()
    {
        return Game::whereIn('status', [0, 1, 2])->where(function (Builder $builder) {
            return $builder->where('first_user_id', $this->id)
                ->orWhere('second_user_id', $this->id);
        })->first();
    }

    /**
     * @return mixed
     */
    public function isOnline()
    {
        return Cache::has('user-online-' . $this->id);
    }

    /**
     * Set api token if null
     */
    public function setApiToken()
    {
        if (is_null($this->api_token)) {
            $this->forceFill([
                'api_token' => Str::random(60),
            ])->save();
        }
    }

    /**
     * Remove api token
     */
    public function removeApiToken()
    {
        $this->forceFill([
            'api_token' => null,
        ])->save();
    }

    public function joinGame()
    {
        $this->update(['status' => 1]);
    }

    public function leftGame()
    {
        $this->update(['status' => 0]);
        $this->notifications()->whereIn('notification_type_id', [2, 3])->delete();
    }

    /**
     * @return bool
     */
    public function isInGame()
    {
        return $this->status == 1;
    }

    /**
     * @return bool
     */
    public function isNotInGame()
    {
        return $this->status == 0;
    }

    /**
     * Set ready field true
     */
    public function toggleReady()
    {
        $this->update(['ready' => !$this->ready]);
    }
}
