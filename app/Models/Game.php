<?php

namespace App\Models;

use App\Http\Resources\ShipResource;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class Game extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'games';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_user_id',
        'second_user_id',
        'ship_list_id',
        'status',
        'first_user_active',
        'rows',
        'cols'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    protected $appends = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firstUser()
    {
        return $this->belongsTo(User::class, 'first_user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function secondUser()
    {
        return $this->belongsTo(User::class, 'second_user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shipList()
    {
        return $this->belongsTo(ShipList::class, 'ship_list_id');
    }

    /**
     * Get the cell for this model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cells()
    {
        return $this->hasMany('App\Models\Cell', 'game_id', 'id');
    }

    /**
     * Get the ship for this model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ships()
    {
        return $this->hasMany('App\Models\Ship', 'game_id', 'id');
    }

    /**
     * @param int $userId
     * @return Collection
     */
    public function getShipsRecords(int $userId): Collection
    {
        return collect(DB::select("
          SELECT * FROM `ships_records` WHERE `game_id` = ? AND `user_id` = ?;
        ", [$this->id, $userId]));
    }

    /**
     * Change users statuses
     */
    public function gameStarted()
    {
        $this->firstUser->joinGame();
        $this->secondUser->joinGame();
    }

    /**
     * Change users statuses
     */
    public function gameEnded()
    {
        $this->firstUser->leftGame();
        $this->secondUser->leftGame();
        $this->game->incrementStatus();
    }

    /**
     * Increment status of game
     */
    public function incrementStatus($num = 1)
    {
        $this->update(['status' => $this->status + $num]);
    }

    /**
     * Increment game status if users ready
     */
    public function isUsersReady(): bool
    {
        if ($this->firstUser->ready &&
            $this->secondUser->ready) {
            $this->incrementStatus();

            $this->firstUser->toggleReady();
            $this->secondUser->toggleReady();

            return true;
        }

        return false;
    }

    /**
     * Toggle first_user_active field
     */
    public function toggleActiveUser()
    {
        $this->update([
            'first_user_active' => !$this->first_user_active
        ]);
    }
}
