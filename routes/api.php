<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->middleware('auth:api')->group(function () {
    Route::resource('notification', 'NotificationController')->only(['store', 'update', 'destroy']);

    Route::resource('/user-ready/', 'ToggleUserReadyController')->only(['store']);

    Route::get('/active-game/{game}', 'ActiveGameController@show');
    Route::put('/active-game/{game}', 'ActiveGameController@update');

    Route::put('/ship-list/{game}', 'ShipListController@update');

    Route::get('/game-field/{game}', 'FieldController@show');
    Route::put('/game-field/{game}', 'FieldController@update');

    Route::get('/game/{game}/fields', 'GameFieldsController@show');

    Route::post('/active-game/shoot', 'ShootController@store');
});
