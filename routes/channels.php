<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

use App\Http\Resources\NotificationResource;
use App\Models\Game;

Broadcast::channel('notifications.user.{id}', function ($user, $id) {
    if ((int)$user->id === (int)$id) {
        return [
            'owner' => new \App\Http\Resources\UserResource($user),
            'notified' => NotificationResource::collection(
                $user->initiatedNotifications()->where('accept', '=', null)->get()
            ),
            'notifications' => NotificationResource::collection(
                $user->notifications()->where('accept', '=', null)->get()
            ),
            'games' => \App\Http\Resources\GameResource::collection(
                \App\Models\Game::where('status', '!=', 3)->get()
            ),
        ];
    }
});

Broadcast::channel('dashboard', function ($user) {
//    TODO GameEnded
    return new App\Http\Resources\UserResource($user);
});

Broadcast::channel('game.configure.{game}', function ($user, $gameId) {
    $game = Game::find($gameId);

    if (is_null($game)) {
        return false;
    }

    if ($game->status != 0) {
        return false;
    }

    if ($user->id == $game->first_user_id ||
        $user->id == $game->second_user_id) {
        return new App\Http\Resources\UserResource($user);
    }

    return false;
});

Broadcast::channel('field.configure.{game}', function ($user, $gameId) {
    $game = Game::find($gameId);

    if (is_null($game)) {
        return false;
    }

    if ($game->status != 1) {
        return false;
    }

    if ($user->id == $game->first_user_id ||
        $user->id == $game->second_user_id) {
        return new App\Http\Resources\UserResource($user);
    }

    return false;
});

Broadcast::channel('active.game.{game}', function ($user, $gameId) {
    $game = Game::find($gameId);

    if (is_null($game)) {
        return false;
    }

    if ($game->status != 2) {
        return false;
    }

    if ($user->id == $game->first_user_id ||
        $user->id == $game->second_user_id) {
        return new App\Http\Resources\UserResource($user);
    }

    return false;
});

Broadcast::channel('watch.game.{game}', function ($user, $gameId) {
    $game = Game::find($gameId);

    return $game ?? null;
});
