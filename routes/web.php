<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Auth')->group(function () {
    Route::post('/login', 'AuthController@login');
    Route::get('/login', 'AuthController@showLoginForm')->name('login');
    Route::post('/logout', 'AuthController@logout')->name('logout')->middleware('logout');
});

Route::namespace('Web')->middleware('auth:web')->group(function () {
	Route::get('/{any}', 'DashboardController@index')->name('dashboard.index')->where('any', '.*');
});

//Route::namespace('Web')->middleware('auth:web')->group(function () {
//    Route::resource('/active-game', 'ActiveGameController')->only(['index']);
//});
