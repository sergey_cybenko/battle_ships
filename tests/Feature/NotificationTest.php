<?php

namespace Tests\Feature;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class NotificationTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function an_authenticated_user_can_invite_other_online_single_user()
    {
        $second_user = $this->sendInvite()[1];

        $this->assertEquals(1, $second_user->fresh()->notifications()->count());
        Cache::flush();
    }

    /** @test */
    public function an_authenticated_user_can_delete_his_invitation()
    {
        $first_user = $this->sendInvite()[0];

        $this->delete(route('notification.destroy', [
            'id' => $first_user->initiatedNotifications()->first()->id
        ]));

        $this->assertEquals(0, $first_user->initiatedNotifications()->count());
        Cache::flush();
    }

    /** @test */
    public function an_invited_user_can_denied_invitation()
    {
        $second_user = $this->sendInvite()[1];

        $this->put(
            route('notification.update', ['id' => $second_user->fresh()->notifications[0]->id]),
            [
                'accept' => false
            ]
        );

        $this->assertEquals(0, $second_user->notifications()->where('accept', null)->count());
        Cache::flush();
    }

    /** @test */
    public function an_invited_user_can_accept_invitation()
    {
        [$first_user, $second_user] = $this->sendInvite();

        $this->put(
            route('notification.update', ['id' => $second_user->fresh()->notifications[0]->id]),
            [
                'accept' => true
            ]
        );

        $this->assertEquals(0, $second_user->notifications()->where('accept', null)->count());
        $this->assertEquals(1, $second_user->fresh()->status);
        $this->assertEquals(1, $first_user->fresh()->status);
        $this->assertEquals(1, $first_user->fresh()->createdGames()->count());
        $this->assertEquals(1, $second_user->fresh()->invitedGames()->count());
        Cache::flush();
    }

    /** @test */
    public function invited_user_can_not_be_invited_twice()
    {
        [$first_user, $second_user] = $this->sendInvite();


        $this->post(
            route('notification.store'),
            [
                'notifiable_user_id' => $second_user->id,
            ],
            [
                'Accept' => 'application/json, text/plain, */*'
            ]
        )->assertStatus(422);
        Cache::flush();
    }

    /** @test */
    public function offline_user_can_not_be_invited()
    {
        $this->setNotificationTypes();
        $first_user = $this->getOnlineUser();
        $second_user = factory(User::class)->create();

        $this->post(
            route('notification.store'),
            [
                'notifiable_user_id' => $second_user->id,
            ],
            [
                'Accept' => 'application/json, text/plain, */*'
            ]
        )->assertStatus(422);
        Cache::flush();
    }

    /** @test */
    public function user_can_not_invite_hisself()
    {
        $user = $this->getOnlineUser();

        $this->post(
            route('notification.store'),
            [
                'notifiable_user_id' => $user->id,
            ],
            [
                'Accept' => 'application/json, text/plain, */*'
            ]
        )->assertStatus(422);
        Cache::flush();
    }

    /** @test */
    public function iser_in_a_game_can_not_be_invited()
    {
        $second_user = $this->getOnlineUser();
        $first_user = $this->getOnlineUser();
        $second_user->update(['status' => 1]);

        $this->post(
            route('notification.store'),
            [
                'notifiable_user_id' => $second_user->id,
            ],
            [
                'Accept' => 'application/json, text/plain, */*'
            ]
        )->assertStatus(422);
        Cache::flush();
    }

    /** @test */
    public function unauthenticated_user_can_not_see_any_page()
    {
        $this->post(
            route('notification.store'),
            [],
            [
                'Accept' => 'application/json, text/plain, */*'
            ]
        )->assertStatus(401);
        $this->put(
            route('notification.update', ['id' => 1]),
            [],
            [
                'Accept' => 'application/json, text/plain, */*'
            ]
        )->assertStatus(401);
        $this->put(
            route('notification.destroy', ['id' => 1]),
            [],
            [
                'Accept' => 'application/json, text/plain, */*'
            ]
        )->assertStatus(401);
    }

    /**
     * Generate notification types
     */
    protected function setNotificationTypes()
    {
        (new \DatabaseSeeder())->run();
    }

    /**
     * @param String $driver
     * @return User
     */
    protected function getOnlineUser(string $driver = 'api'): User
    {
        $user = factory(User::class)->create();
        $this->be($user, $driver);
        $user->setApiToken();
        Cache::put(
            'user-online-' . $user->id,
            true,
            Carbon::now()->addMinutes(1)
        );
        return $user;
    }

    /**
     * @return array
     */
    protected function sendInvite(): array
    {
        $this->setNotificationTypes();

        $second_user = $this->getOnlineUser();
        $first_user = $this->getOnlineUser();

        $this->post(
            route('notification.store'),
            [
                'notifiable_user_id' => $second_user->id,
            ]
        );
        return [$first_user, $second_user];
    }
}
