@extends('layouts.game')

@section('script.top')
    <script>
        window.api_token = '{{ $user->api_token }}'
        window.user_id = parseInt('{{ $user->id }}')
        window.game_id = parseInt('{{ $user->active_game->id }}')
    </script>
    <script src="{{ asset('js/game.js') }}" defer></script>
@endsection

@section('main')
    <body-component/>
@endsection

