@extends('layouts.app')

@section('script.top')
    <script>
        window.api_token = '{{ $user->api_token }}'
        window.user_id = parseInt('{{ $user->id }}')
    </script>
    <script src="{{ asset('js/dashboard.js') }}" defer></script>
@endsection

@section('content')
    <main-component></main-component>
@endsection