require('./bootstrap');

window.Vue = require('vue');

Vue.component('body-component', require('./components/Game/Body.vue').default);
Vue.component('alert-component', require('./components/Alert.vue').default);
Vue.component('nav-bar-component', require('./components/Game/NavBar.vue').default);
Vue.component('card-component', require('./components/Card.vue').default);
Vue.component('field-component', require('./components/Game/Field.vue').default);
Vue.component('messages-component', require('./components/Game/Messages.vue').default);

const app = new Vue({
    el: '#app',
});
