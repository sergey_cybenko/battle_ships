require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router'

import DashboardMain from './components/Dashboard/DashboardMain.vue'
import FieldConfigureBody from './components/FieldConfigure/Body.vue'
import GameConfigureMain from './components/GameConfigure/GameConfigureMain.vue'
import GameBody from './components/Game/GameBody.vue'

Vue.use(VueRouter);

Vue.component('alert-component', require('./components/Alert.vue').default);
Vue.component('card-component', require('./components/Card.vue').default);
Vue.component('nav-bar-component', require('./components/NavBar.vue').default);


Vue.component('main-component', DashboardMain);
Vue.component('dashboard-card-component', require('./components/Dashboard/Card.vue').default);
Vue.component('user-list-item', require('./components/Dashboard/UserListItem.vue').default);
Vue.component('game-list-item', require('./components/Dashboard/GameListItem.vue').default);

Vue.component('field-body-component', FieldConfigureBody);
Vue.component('field-component', require('./components/FieldConfigure/Field.vue').default);
Vue.component('ship-template', require('./components/FieldConfigure/ShipTemplate.vue').default);
Vue.component('ship-drag-enter', require('./components/FieldConfigure/ShipDragEnter.vue').default);

Vue.component('game-configure-body-component', GameBody);
Vue.component('game-configure-main-component', require('./components/GameConfigure/GameConfigureMain.vue').default);
Vue.component('size-input', require('./components/GameConfigure/SizeInput.vue').default);
Vue.component('ship-template-input', require('./components/GameConfigure/ShipTemplateInput.vue').default);

Vue.component('game-body-component', GameBody);
Vue.component('game-nav-bar-component', require('./components/Game/NavBar.vue').default);
Vue.component('game-field-component', require('./components/Game/Field.vue').default);
Vue.component('messages-component', require('./components/Game/Messages.vue').default);

const routes = [
    {path: '/', component: DashboardMain},
    {path: '/game-configure', component: GameConfigureMain},
    {path: '/field-configure', component: FieldConfigureBody},
    {path: '/game', component: GameBody},
]

const router = new VueRouter({
    mode: 'history',
    routes
})

const app = new Vue({
    el: '#app',
    router,
});
