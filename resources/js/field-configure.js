require('./bootstrap');

window.Vue = require('vue');

Vue.component('main-component', require('./components/FieldConfigure/Main.vue').default);
Vue.component('alert-component', require('./components/Alert.vue').default);
Vue.component('body-component', require('./components/FieldConfigure/Body.vue').default);
Vue.component('card-component', require('./components/Card.vue').default);
Vue.component('field-component', require('./components/FieldConfigure/Field.vue').default);
Vue.component('ship-template', require('./components/FieldConfigure/ShipTemplate.vue').default);
Vue.component('ship-drag-enter', require('./components/FieldConfigure/ShipDragEnter.vue').default);
Vue.component('nav-bar-component', require('./components/NavBar.vue').default);

const app = new Vue({
    el: '#app',
});
