require('./bootstrap');

window.Vue = require('vue');

Vue.component('body-component', require('./components/GameConfigure/Body.vue').default);
Vue.component('alert-component', require('./components/Alert.vue').default);
Vue.component('nav-bar-component', require('./components/NavBar.vue').default);
Vue.component('main-component', require('./components/GameConfigure/Main.vue').default);
Vue.component('card-component', require('./components/Card.vue').default);
Vue.component('size-input', require('./components/GameConfigure/SizeInput.vue').default);
Vue.component('ship-template-input', require('./components/GameConfigure/ShipTemplateInput.vue').default);

const app = new Vue({
    el: '#app',
});
