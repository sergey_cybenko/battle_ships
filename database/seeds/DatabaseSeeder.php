<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->createMessages();

        $this->createShipTemplates();

        $this->createNotificationTypes();
    }

    protected function createNotificationTypes(): void
    {
        $notificationTypes = [
            'invite',
            'surrender',
            'draw'
        ];

        foreach ($notificationTypes as $type) {
            factory(\App\Models\NotificationType::class)->create([
                'type' => $type
            ]);
        }
    }

    protected function createShipTemplates(): void
    {
        $shipTemplates = [
            'Linkor' => 4,
            'Craiser' => 3,
            'SmallCraiser' => 2,
            'Buxir' => 1,
        ];

        foreach ($shipTemplates as $name => $size) {
            factory(\App\Models\ShipTemplate::class)->create([
                'name' => $name,
                'size' => $size,
            ]);
        }
    }

    protected function createMessages()
    {
        $messages = [
            'Miss',
            'Hit',
            'Destroy'
        ];

        foreach ($messages as $content) {
            \App\Models\Message::create([
                'content' => $content
            ]);
        }
    }
}
