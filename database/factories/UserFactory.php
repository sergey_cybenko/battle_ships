<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cell;
use App\Models\Game;
use App\Models\NotificationType;
use App\Models\ShipList;
use App\Models\ShipTemplate;
use App\Models\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'remember_token' => Str::random(10),
    ];
});

$factory->define(Game::class, function (Faker $faker) {
    return [
        'first_user_id' => factory(User::class)->create()->id,
        'second_user_id' => factory(User::class)->create()->id,
        'ship_list_id' => factory(\App\Models\Ship::class)->create()->id,
        'rows' => 10,
        'cols' => 10
    ];
});

$factory->define(NotificationType::class, function (Faker $faker) {
    return [
        'type' => $faker->word
    ];
});

$factory->define(ShipTemplate::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'size' => $faker->numberBetween(1, 5),
    ];
});

$factory->define(ShipList::class, function (Faker $faker) {
    $data = ShipTemplate::get(['id', 'name', 'size'])->all();

    array_map(function ($item) {
        $item['count'] = 0;
    }, $data);

    return [
        'data' => json_encode($data),
    ];
});

$factory->define(Cell::class, function (Faker $faker) {
    return [
        'row' => $faker->numberBetween(1, 10),
        'col' => $faker->numberBetween(1, 10),
        'user_id' => $faker->numberBetween(1, 10),
        'game_id' => $faker->numberBetween(1, 10),
        'ship_id' => $faker->numberBetween(1, 10)
    ];
});
