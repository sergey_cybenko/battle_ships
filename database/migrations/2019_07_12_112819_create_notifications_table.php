<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('notification_type_id');
            $table->unsignedInteger('initiated_user_id');
            $table->unsignedInteger('notifiable_user_id');
            $table->boolean('accept')->nullable();
            $table->timestamps();

            $table->foreign('notification_type_id')->references('id')->on('notification_types');
            $table->foreign('initiated_user_id')->references('id')->on('users');
            $table->foreign('notifiable_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
