<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class ShipsRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW `ships_records`
                AS
            SELECT `s`.`id` AS `id`, `s`.`ship_template_id` AS `ship_template_id`, `s`.`game_id` AS `game_id`, `st`.`name` AS `name`,
                `st`.`size` AS `size`, `s`.`user_id` AS `user_id`, COUNT(`c`.`id`) AS `count`
            FROM `ships` AS `s` 
            JOIN`ship_templates` AS `st` ON `st`.`id` = `s`.`ship_template_id`
            LEFT JOIN `cells` AS `c` ON `c`.`ship_id` = `s`.`id` AND `c`.`user_id` = `s`.`user_id`
            GROUP BY `id`, `name`, `size`;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW `ships_records`;
        ");
    }
}
