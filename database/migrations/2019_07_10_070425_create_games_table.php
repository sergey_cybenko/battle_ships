<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('first_user_id');
            $table->unsignedInteger('second_user_id');
            $table->unsignedInteger('ship_list_id');
            $table->integer('status')->default(0);
            $table->boolean('first_user_active')->nullable();
            $table->integer('rows')->default(10);
            $table->integer('cols')->default(10);
            $table->timestamps();

            $table->foreign('ship_list_id')->references('id')->on('ship_lists');
            $table->foreign('first_user_id')->references('id')->on('users');
            $table->foreign('second_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('games');
    }
}
