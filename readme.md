
## Battle Ships

##### Required
- redis
- mysql

##### Install
    cp .env.example .env

    composer install
    php artisan key:gen
    php artisan migrate
    php artisan db:seed
    
.env

    DB_DATABASE=
    DB_USERNAME=
    DB_PASSWORD=
    
---   
    npm install
    npm run dev

    
##### Start
    laravel-echo-server start

---
    php artisan queue:work database --tries=1 --queue=notifications,default --sleep=0


---
    php artisan serve
